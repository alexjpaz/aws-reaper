const {
  filterExpiredStacks,
  removeExpiredStacks
} = require('./reaper');

const { expect } = require('chai');

describe('filterExpiredStacks', () => {

  it('should return expired stacks', () => {
    const now = new Date("2018-06-01");
    const result = filterExpiredStacks([
      {
        ResourceARN: "arn:fake1",
        Tags: [{
          Key: "expiresAt",
          Value: "2018-05-23"
        }]
      },{
        ResourceARN: "arn:fake2",
        Tags: [{
          Key: "expiresAt",
          Value: "2018-05-23"
        }]
      }
    ], now);

    expect(result).to.have.length(2);
  });

  it('should return expired stacks only', () => {
    const now = new Date("2018-06-01");
    const result = filterExpiredStacks([
      {
        ResourceARN: "arn:fake1",
        Tags: [{
          Key: "expiresAt",
          Value: "2018-05-23"
        }]
      },{
        ResourceARN: "arn:fake2",
        Tags: [{
          Key: "expiresAt",
          Value: "2018-07-01"
        }]
      }
    ], now);

    expect(result).to.have.length(1);
    expect(result[0].ResourceARN).to.eql("arn:fake1");
  });

  it('should allow empty arrays', () => {
    const result = filterExpiredStacks([]);

    expect(result).to.have.length(0);
  });

 it('should return expired stacks only', () => {
   const now = new Date("2018-06-01");
   const result = filterExpiredStacks([
     {
       ResourceARN: "arn:fake1",
       Tags: [{
         Key: "expiresAt",
         Value: "2018-05-23"
       }]
     },{
       ResourceARN: "arn:fake2",
       Tags: [{
         Key: "SomeOtherKey",
         Value: ""
       }]
     }
   ], now);

   expect(result).to.have.length(1);
   expect(result[0].ResourceARN).to.eql("arn:fake1");
 });

  it('should allow different formats', () => {
    const now = new Date("2018-06-01");
    const result = filterExpiredStacks([
      {
        ResourceARN: "arn:fake1",
        Tags: [{
          Key: "expiresAt",
          Value: "2018-05-23"
        }]
      },{
        ResourceARN: "arn:fake2",
        Tags: [{
          Key: "expiresAt",
          Value: "2018-05-23T17:01:53.728Z"
        }]
      }
    ], now);

    expect(result).to.have.length(2);
  });
});
