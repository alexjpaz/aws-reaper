const AWS = require('aws-sdk');
const cloudformation = new AWS.CloudFormation();

const logger = console // TODO

const getEphemerialCloudformationStacks = async () => {

  const tagging = new AWS.ResourceGroupsTaggingAPI();

  var params = {
    ResourceTypeFilters: [
      "cloudformation"
    ],
    TagFilters: [
      {
        Key: "expiresAt",
      }
    ]
  };

  const { ResourceTagMappingList } = await tagging.getResources(params).promise();

  return ResourceTagMappingList;
}

const filterExpiredStacks = (stackList, now = new Date()) => {
  return stackList.filter((stack) => {
    const { Tags } = stack;

    return Tags
      .filter(t => t.Key === "expiresAt")
      .some(t => (now > new Date(t.Value)))
    ;
  });
  ;
};

var params = {
  LogicalResourceId: 'STRING_VALUE', /* required */
  StackName: 'STRING_VALUE' /* required */
};
cloudformation.describeStackResource(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log(data);           // successful response
});

const getRetainResourcesForStack = async (StackName) => {
  let retain = [];

  var params = {
    StackName,
  };

  const { StackResourceSummaries } = await cloudformation.listStackResources(params).promise();

  retain = StackResourceSummaries
    .filter((s) => s.ResourceType === "AWS::S3::Bucket")
    .map((s) => s.LogicalResourceId);

  return retain;
};

const removeExpiredStacks = async (stackList) => {

  await Promise.all(stackList.map(async (stack) => {
    const StackName = stack.ResourceARN.split('/')[1];

    logger.info("Deleting stack", StackName, stack);

    let RetainResources = await getRetainResourcesForStack(StackName)

    const params = {
      StackName,
      RetainResources
    };

    let response

    try {
      response = await cloudformation.deleteStack(params).promise();
    } catch(e) {
      logger.warn("Could not delete stack. Trying another method", e);
      params.RetainResources = null;
      response = await cloudformation.deleteStack(params).promise();
    }

    logger.info("deleted stack", JSON.stringify(response));
  }));
};

const main = async () => {
  const stackList = await getEphemerialCloudformationStacks();

  const now = new Date();

  const expiredStacks = filterExpiredStacks(stackList);

  await removeExpiredStacks(stackList);
};

module.exports = {
  main,
  getEphemerialCloudformationStacks,
  filterExpiredStacks,
};
