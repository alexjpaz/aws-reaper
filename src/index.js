const { main } = require('./reaper');

exports.handler = async (event, context) => {
  const AWS = require('aws-sdk');
  AWS.config.update({region:'us-east-1'}); // TODO - maybe get from the event context

  try {
    await main();
  } catch(e) {
    throw e;
  }
};
